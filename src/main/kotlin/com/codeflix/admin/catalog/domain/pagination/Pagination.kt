package com.codeflix.admin.catalog.domain.pagination

@JvmRecord
data class Pagination<T>(
    val currentPage: Int,
    val perPage: Int,
    val total: Long,
    val items: List<T>
) {
}